<?php
use PHPUnit\Framework\TestCase;

final class EmployeeTest extends TestCase
{
    public function testNoJobsPerformed()
    {
        $deadBeat = new Manager();
        $this->assertEquals(0, $deadBeat->jobsCompletedCount());
        $this->assertEquals(0, $deadBeat->jobsSkippedCount());
    }

    public function testThreeJobsPerformedNoneSkippedByManager()
    {
        $productiveManager = new Manager();
        $productiveManager->performJob("MANAGE");
        $productiveManager->performJob("MARKET");
        $productiveManager->performJob("SELL");
        $this->assertEquals(3, $productiveManager->jobsCompletedCount());
        $this->assertEquals(0, $productiveManager->jobsSkippedCount());
    }

    public function testOneJobDoneOneJobSkippedByManager()
    {
        $unbusyManager = new Manager();
        $unbusyManager->performJob("TEST");
        $unbusyManager->performJob("SELL");
        $this->assertEquals(1, $unbusyManager->jobsCompletedCount());
        $this->assertEquals(1, $unbusyManager->jobsSkippedCount());
    }

    public function testNoJobsDone()
    {
        $deadBeat = new Programmer();
        $this->assertEquals(0, $deadBeat->jobsDoneCount());
        $this->assertEquals(0, $deadBeat->jobsSkippedCount());
    }

    public function testThreeJobsPerformedNoneSkippedByProgrammer()
    {
        $productiveProgrammer = new Programmer();
        $productiveProgrammer->performJob("TEST");
        $productiveProgrammer->performJob("PROGRAM");
        $productiveProgrammer->performJob("INTEGRATE");
        $this->assertEquals(3, $productiveProgrammer->jobsDoneCount());
        $this->assertEquals(0, $productiveProgrammer->jobsSkippedCount());
    }

    public function testOneJobDoneOneJobSkippedByProgrammer()
    {
        $unbusyProgrammer = new Programmer();
        $unbusyProgrammer->performJob("TEST");
        $unbusyProgrammer->performJob("SELL");
        $this->assertEquals(1, $unbusyProgrammer->jobsDoneCount());
        $this->assertEquals(1, $unbusyProgrammer->jobsSkippedCount());
    }
}