<?php

class Manager extends Employee
{
    private $jobsCompleted = 0;
    private $jobsSkipped = 0;

    public function jobsCompletedCount()
    {
        return $this->jobsCompleted;
    }

    public function jobsSkippedCount()
    {
        return $this->jobsSkipped;
    }

    public function performJob($job)
    {
        if(in_array($job, $this->responsibilities())) {
            $this->jobsCompleted++;
        } else {
            $this->jobsSkipped++;
        }
    }

    private function responsibilities()
    {
        return ["MANAGE", "MARKET", "SELL"];
    }
}