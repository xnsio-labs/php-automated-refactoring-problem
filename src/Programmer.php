<?php

class Programmer extends Employee
{
    private $jobsDone = 0;
    private $jobsSkipped = 0;

    public function jobsDoneCount()
    {
        return $this->jobsDone;
    }

    public function jobsSkippedCount()
    {
        return $this->jobsSkipped;
    }

    public function performJob($job)
    {
        $acceptableWork = ["TEST", "PROGRAM", "INTEGRATE", "DESIGN"];
        if(in_array($job, $acceptableWork)) {
            $this->jobsDone++;
        } else {
            $this->jobsSkipped++;
        }
    }
}